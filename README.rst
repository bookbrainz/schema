schema
======

This package contains the SQLAlchemy models mapping the BookBrainz database
schema to Python objects. It is likely to change considerably during the early
stages of development.

Installation
------------

First, install all of the required modules. You can do this with pip, as
follows:

    pip install -r requirements.txt

Then, run setup.py, to install the bbschema package:

    python setup.py install

You'll probably need to prefix these commands with "sudo" to get anywhere!
